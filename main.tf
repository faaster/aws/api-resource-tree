
locals {
  #
  # Remove the method table
  paths = { for k, v in var.path_tree : k => v if k != "->" }

  # Prepare map of methods updated with resource IDs and full paths
  methods_updated = {
    # fix up method list with resource pointers
    for k, v in var.path_tree :
    k => ([for m in v : merge(m, {
      api_res_id = var.parent_id,
      fullpath   = var.parent_fullpath
    })]) if(k == "->")
  }

  # Extract the method list
  methods_listing = lookup(local.methods_updated, "->", [])
}

resource "aws_api_gateway_resource" "path" {
  for_each = local.paths

  rest_api_id = var.api_id
  parent_id   = var.parent_id
  path_part   = each.key
}
