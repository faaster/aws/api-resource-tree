
output "path_resources" {
  #
  # 
  value = merge(
    local.methods_updated,
    {
      # fix up nested resources with each of their resource pointers
      for k, v in var.path_tree :
      k => { "&" = aws_api_gateway_resource.path[k].id } if(k != "->")
    }
  )
}

output "methods_list" {
  value = local.methods_listing
}
