
module "path_part" {
  for_each = local.paths

  source          = "./modules/api_resource_sub_tree"
  api_id          = var.api_id
  parent_id       = aws_api_gateway_resource.path[each.key].id
  parent_fullpath = "${var.parent_fullpath}/${aws_api_gateway_resource.path[each.key].path_part}"
  path_tree       = each.value
}

