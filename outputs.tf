
output "path_resources" {
  # Example:
  # path_resources = {
  #   "hello" = {
  #     "&" = {
  #       "&" = "rhxxxo"
  #       "->" = [
  #         {
  #           "api_res_id" = "rhxxxo"
  #           "fn_mem" = 256
  #           "fn_name" = "post_hello"
  #           "fn_timeout" = 3
  #           "fullpath" = "/hello"
  #           "method" = "POST"
  #         },
  #       ]
  #       "{id}" = {
  #         "&" = {
  #           "&" = "76xxxe"
  #           "->" = [
  #             {
  #               "api_res_id" = "76eu3e"
  #               "fn_mem" = 256
  #               "fn_name" = "get_hello_id"
  #               "fn_timeout" = 3
  #               "fullpath" = "/hello/{id}"
  #               "method" = "GET"
  #             },
  #             {
  #               "api_res_id" = "76eu3e"
  #               "fn_mem" = 256
  #               "fn_name" = "del_hello_id"
  #               "fn_timeout" = 3
  #               "fullpath" = "/hello/{id}"
  #               "method" = "DELETE"
  #             },
  #           ]
  #         }
  #       }
  #     }
  #   }
  # }

  description = "A map based on the `path_tree` modified to add `api_res_id` and `fullpath` properties based on the created API resource paths."
  #
  # 
  value = merge(
    local.methods_updated,
    {
      # fix up nested resources with each of their resource pointers
      for k, v in var.path_tree :
      k => { "&" = merge(
        { "&" = aws_api_gateway_resource.path[k].id },
        module.path_part[k].path_resources)
      } if(k != "->")
    }
  )
}

output "methods_list" {
  # Example:
  # methods_list = [
  #   {
  #     "api_res_id" = "rhxxxo"
  #     "fn_mem" = 256
  #     "fn_name" = "post_hello"
  #     "fn_timeout" = 3
  #     "fullpath" = "/hello"
  #     "method" = "POST"
  #   },
  #   {
  #     "api_res_id" = "76xxxe"
  #     "fn_mem" = 256
  #     "fn_name" = "get_hello_id"
  #     "fn_timeout" = 3
  #     "fullpath" = "/hello/{id}"
  #     "method" = "GET"
  #   },
  #   {
  #     "api_res_id" = "76xxxe"
  #     "fn_mem" = 256
  #     "fn_name" = "del_hello_id"
  #     "fn_timeout" = 3
  #     "fullpath" = "/hello/{id}"
  #     "method" = "DELETE"
  #   },
  # ]
  description = "A flat list of the method endpoints; this can be used to create the necessary methods and functions."
  value = flatten([
    local.methods_listing,
    [for k, v in var.path_tree : module.path_part[k].methods_list if(k != "->")]
  ])
}

output "path_method_map" {
  # Example:
  # path_method_map = {
  #   "/hello" = {
  #     "api_res_id" = "j8uyqu"
  #     "http_methods" = [
  #       "POST",
  #     ]
  #   }
  #   "/hello/{id}" = {
  #     "api_res_id" = "zeccjt"
  #     "http_methods" = [
  #       "GET",
  #       "DELETE",
  #     ]
  #   }
  # }  
  description = "A map of API HTTP methods and resource info per full path."
  value = merge(
    length(local.methods_listing) > 0
    ? { (var.parent_fullpath == "" ? "/" : var.parent_fullpath) = {
      http_methods = local.methods_listing[*].method
      api_res_id   = var.parent_id
      }
    }
    : {},
    merge([for mp in values(module.path_part) : mp.path_method_map]...)
  )
}
