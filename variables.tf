
variable "api_id" {
  description = "The ID of the API Gateway REST API under which this path is deployed."
}

variable "parent_id" {
  description = "The ID of the resource that will be the parent path under which this path is deployed. Can be the `root_id` of an API or the `id` of another resource."
}

variable "parent_fullpath" {
  description = "The full path of the parent resource; if this `parent_id` is the root of the API, then specify an empty string as value. Defaults to an empty string."
  default     = ""
}

variable "path_tree" {
  description = "The resource path tree is specified using a structured map respresentation of the REST API. See README for example."

}
