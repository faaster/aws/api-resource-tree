# FaasTer/AWS API Resource Tree Module

> Module works; documentation is still a work in progress.

This is a Terraform module that can be used to define and create AWS API Gateway "Resource" paths by specifying a single nested API description.

Since Terraform modules don't support recursion, this module a fixed number of nested submodules. Since most of the module configuration is identical, and to reduce the pain of keeping copies of the same files up to date, soft links are used to share all but one of the configuration files.

- [Changelog](#changelog)
- [Using the Module](#using-the-module)
  - [Method Map Example](#method-map-example)
- [External References](#external-references)
- [Module Reference](#module-reference)
  - [Requirements](#requirements)
  - [Providers](#providers)
  - [Modules](#modules)
  - [Resources](#resources)
  - [Inputs](#inputs)
  - [Outputs](#outputs)

## Changelog

See the [repository tags list](https://gitlab.com/faaster/aws/api-resource-tree/-/tags) for the changelog.

## Using the Module

```hcl
module "my_api_paths" {
  source = "git::https://gitlab.com/faaster/aws/api-resource-tree.git"

  api_id    = aws_api_gateway_rest_api.this.id
  parent_id = aws_api_gateway_rest_api.this.root_resource_id
  path_tree = local.my_method_map   # See example in README below

  # parent_fullpath defaults to "" since defining from the root of the API
}
```

### Method Map Example

This example defines an API method map (as a `local` in Terraform) for the following REST API endpoints:
- `POST /account`
- `DELETE /account/{aid}`
- `GET /account/{aid}/details`

```hcl
locals {
    my_api_method_map = {
        "account" = {
            "->" = [
                { method = "POST" }
            ]
            "{aid}" = {
                "->" = [
                    { method = "DELETE" }
                ]
                "details" = {
                    "->" = [
                        { method = "GET" }
                    ]
                }
            }
        }
    }
}
```

## External References

1. Terraform Best Practices | [Changelog Specification](https://www.terraform.io/docs/extend/best-practices/versioning.html#changelog-specification)

## Module Reference

### Requirements

| Name                                                                      | Version   |
| ------------------------------------------------------------------------- | --------- |
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14   |
| <a name="requirement_aws"></a> [aws](#requirement\_aws)                   | >= 3.30.0 |

### Providers

| Name                                              | Version   |
| ------------------------------------------------- | --------- |
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.30.0 |

### Modules

| Name                                                              | Source                          | Version |
| ----------------------------------------------------------------- | ------------------------------- | ------- |
| <a name="module_path_part"></a> [path\_part](#module\_path\_part) | ./modules/api_resource_sub_tree |         |

### Resources

| Name                                                                                                                              | Type     |
| --------------------------------------------------------------------------------------------------------------------------------- | -------- |
| [aws_api_gateway_resource.path](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_resource) | resource |

### Inputs

| Name                                                                              | Description                                                                                                                                            | Type     | Default | Required |
| --------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | -------- | ------- | :------: |
| <a name="input_api_id"></a> [api\_id](#input\_api\_id)                            | The ID of the API Gateway REST API under which this path is deployed.                                                                                  | `any`    | n/a     |   yes    |
| <a name="input_parent_fullpath"></a> [parent\_fullpath](#input\_parent\_fullpath) | The full path of the parent resource; if this `parent_id` is the root of the API, then specify an empty string as value. Defaults to an empty string.  | `string` | `""`    |    no    |
| <a name="input_parent_id"></a> [parent\_id](#input\_parent\_id)                   | The ID of the resource that will be the parent path under which this path is deployed. Can be the `root_id` of an API or the `id` of another resource. | `any`    | n/a     |   yes    |
| <a name="input_path_tree"></a> [path\_tree](#input\_path\_tree)                   | The resource path tree is specified using a structured map respresentation of the REST API. See README for example.                                    | `any`    | n/a     |   yes    |

### Outputs

| Name                                                                                  | Description                                                                                                                    |
| ------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| <a name="output_methods_list"></a> [methods\_list](#output\_methods\_list)            | A flat list of the method endpoints; this can be used to create the necessary methods and functions.                           |
| <a name="output_path_method_map"></a> [path\_method\_map](#output\_path\_method\_map) | A map of API HTTP methods and resource info per full path.                                                                     |
| <a name="output_path_resources"></a> [path\_resources](#output\_path\_resources)      | A map based on the `path_tree` modified to add `api_res_id` and `fullpath` properties based on the created API resource paths. |
